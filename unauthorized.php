<?php require_once './utils/session_helper.php'; ?>
<?php
    my_session_start();
    if(!empty($_SESSION['user_id'])) {
        $user_id = $_SESSION['user_id'];
	  }
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="icon" type="image/png" href="./dependancies/MaterialKitv2.0.3/assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
  Food On The Go - Unauthorized :(
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="./dependancies/MaterialKitv2.0.3/assets/css/material-kit.css?v=2.0.3" rel="stylesheet" />
  <!-- Custom CSS Files  -->
  <link href="./dependancies/MaterialKitv2.0.3/assets/css/style.css" rel="stylesheet" />
</head>

<body class="index-page sidebar-collapse">
  <?php require_once './utils/navigation.php'?>
  <div class="page-header header-filter clear-filter" data-parallax="true" style="background-color: #BCAAA4;">
        <div class="container">
          <div class="card card-nav-tabs">
          <div class="card-header card-header-danger text-center">
            401 Unauthorized
          </div>
          <div class="card-body">
            <h2 class="text-center">Something Went Wrong :(</h2>
            <h4 class="text-center"><small>Try to login first!</small></p>
          </div>
        </div>
      </div>
      </div>

  <?php require_once './utils/footer.php' ?>

  <!--   Modals   -->
  <?php include "./modals/login.php"; ?>
  <?php include "./modals/register.php"; ?>

  <!--   Core JS Files   -->
  <script src="./dependancies/MaterialKitv2.0.3/assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="./dependancies/MaterialKitv2.0.3/assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="./dependancies/MaterialKitv2.0.3/assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
  <script src="./dependancies/MaterialKitv2.0.3/assets/js/plugins/moment.min.js"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="./dependancies/MaterialKitv2.0.3/assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="./dependancies/MaterialKitv2.0.3/assets/js/material-kit.js?v=2.0.3" type="text/javascript"></script>

  <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>


  <script>
    $(document).ready(function() {
      $("#loginForm").validate();
      $("#signupForm").validate();
  });
</script>
<script>
$(function () {
    $('input[name="supplcode"]').hide();
    $('input[name="supplcode"]').prop('required',false);

    //show it when the checkbox is clicked
    $('input[name="suppl_check"]').on('click', function () {
        if ($(this).prop('checked')) {
            $('input[name="supplcode"]').fadeIn();
            $('input[name="supplcode"]').prop('required',true);
        } else {
            $('input[name="supplcode"]').hide();
            $('input[name="supplcode"]').prop('required',false);
        }
    });
});
</script>
</body>

</html>
