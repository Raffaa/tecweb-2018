<?php require_once './utils/session_helper.php'; ?>
<?php require_once './utils/db_connect.php'; ?>
<?php
    my_session_start();

    function categoryName($category_id) {
      switch($category_id){
        case 1:
              return 'Starter';
              break;
        case 2:
        return 'First Course';
        break;
        case 3:
        return 'Second Course';
        break;
        case 4:
        return 'Dessert';
        break;
        case 5:
        return 'Drink';
        break;
      }
    }
    ?>
<?php
if (isset($_SESSION['user_id'])) {
  ?>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="./dependancies/MaterialKitv2.0.3/assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
    Food On The Go - Cart
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="./dependancies/MaterialKitv2.0.3/assets/css/material-kit.css?v=2.0.3" rel="stylesheet" />
    <!-- Custom CSS Files  -->
    <link href="./dependancies/MaterialKitv2.0.3/assets/css/style.css" rel="stylesheet" />
  </head>
  <body class="profile-page sidebar-collapse">
  <?php require_once './utils/navigation.php'?>
  <div class="page-header header-filter clear-filter" data-parallax="true" style="background-image: url('./dependancies/MaterialKitv2.0.3/assets/img/cover_cart.png');"></div>
<div class="main main-raised">
        <div class="container">
          <div class="space-50"></div>
            <div class='section section-basic' id="cartTable">
              <div class='text-center'>
          <?php
          $cookie = isset($_COOKIE['cart_items_cookie']) ? $_COOKIE['cart_items_cookie'] : "";
          $cookie = stripslashes($cookie);
          $saved_cart_items = json_decode($cookie, true);

          $subtotal = 0;

            if(count($saved_cart_items)>0){
              $mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
              foreach($saved_cart_items as $id=>$qt) {
                if($stmt = $mysqli->prepare("SELECT name, price FROM products WHERE product_id = ? LIMIT 1")) {
                  $stmt->bind_param('i', $id); // esegue il bind del parametro '$id'.
                      $stmt->execute(); // esegue la query appena creata.
                      $stmt->store_result();
                      $stmt->bind_result($name, $price);
                      if($stmt->num_rows >= 1) {
                        $stmt->fetch(); ?>
                        <div class="row">
                          <div class="col-md-4 ml-auto mr-auto">
                            <h4><?php echo $name ?></h4>
                          </div>
                          <div class="col-md-2 ml-auto mr-auto">
                            <h5><small class="text-muted"><?php echo $price." €" ?></small></h5>
                          </div>
                          <div class="col-md-2 ml-auto mr-auto">
                            <div class="row-md-4 ml-auto mr-auto">
                            <button class="btn btn-primary btn-link" onclick=<?php echo '"updateQti('.$id.', 1)"'?>>
                              <i class="material-icons">exposure_plus_1</i>
                            </button>
                            <h5><small><?php echo $qt['quantity']; ?></small></h5>
                            <button class="btn btn-primary btn-link" onclick=<?php echo '"updateQti('.$id.', -1)"'?>>
                              <i class="material-icons">exposure_neg_1</i>
                            </button>
                          </div>
                          </div>
                          <div class="col-md-2 ml-auto mr-auto">
                            <button class="btn btn-danger btn-fab btn-fab-mini btn-round" onclick=<?php echo '"removeFromCart('.$id.')"'?>>
                              <i class="material-icons">delete</i>
                            </button>
                          </div>
                        </div>
                        <hr>
                      <?php
                      $subtotal += ($price * $qt['quantity']);
                      } else {
                        die('SQL ERROR :(');
                      }
                      $stmt->free_result();
                    }
                }?>
              </br>
                <div class="row">
                  <div class="col">
                    <h4><?php echo $subtotal." €"?></h4>
                  </div>
                  <div class="col">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#deliveryModal">Checkout</button>
                  </div>
              </div>
                <?php

              } else { ?>
                <div class="col-md-6 ml-auto mr-auto">
                  <h4>Your cart is empty :(</h4>
                </div>
            <?php  }
            ?>
          </div>
          </div>
            <div class="space-50"></div>
      </div>
    </div>
    <?php require_once './utils/footer.php' ?>

    <?php require "./modals/delivery_data.php"; ?>

    <!--   Core JS Files   -->
    <script src="./dependancies/MaterialKitv2.0.3/assets/js/core/jquery.min.js" type="text/javascript"></script>
    <script src="./dependancies/MaterialKitv2.0.3/assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="./dependancies/MaterialKitv2.0.3/assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
    <script src="./dependancies/MaterialKitv2.0.3/assets/js/plugins/moment.min.js"></script>
    <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
    <script src="./dependancies/MaterialKitv2.0.3/assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
    <script src="./dependancies/MaterialKitv2.0.3/assets/js/material-kit.js?v=2.0.3" type="text/javascript"></script>

    <script>
    function removeFromCart(pid) {
      var productId = pid;
      $.ajax({
            type:"GET",
            url:'https://www.foodonthego.altervista.org/utils/cart/carthelper.php/',
            data: {productId: productId, removeProduct: 1},
            success:function(){
                   // refresh badge
                   $("#sectionsNav").load(location.href+" #sectionsNav>*","");
                   $("#cartTable").load(location.href+" #cartTable>*","");

            },
            error: function() {
              alert('AAHAH');
            }
         });
    }

    function updateQti(pid, mode) {
      $.ajax({
        type:"POST",
        url:'https://www.foodonthego.altervista.org/utils/cart/carthelper.php/',
        data: {pid: pid, mode: mode},
        success:function(){
               // refresh badge
               $("#sectionsNav").load(location.href+" #sectionsNav>*","");
               $("#cartTable").load(location.href+" #cartTable>*","");

        },
        error: function() {
          alert('AAHAH');
        }
     });
    }

    </script>

  </body>
  </html>
<?php } else {
  header("Location: https://www.foodonthego.altervista.org/unauthorized.php");
  }
?>
