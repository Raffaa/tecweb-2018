<?php require_once './utils/session_helper.php'; ?>
<?php require_once './utils/db_connect.php'; ?>
<?php
    function emptyCart() {
            // read
      $cookie = $_COOKIE['cart_items_cookie'];
      $cookie = stripslashes($cookie);
      $saved_cart_items = json_decode($cookie, true);

      // remove values
      unset($saved_cart_items);

      // delete cookie value
      unset($_COOKIE["cart_items_cookie"]);

      // empty value and expiration one hour before
      setcookie("cart_items_cookie", "", time() - 3600);

      // enter empty value
      $json = json_encode($saved_cart_items, true);
      setcookie("cart_items_cookie", $json, time() + (86400 * 30), '/'); // 86400 = 1 day
      $_COOKIE['cart_items_cookie']=$json;
    }

    my_session_start();

    $address = $_POST['address'];
    $city = $_POST['city'];
    $postal_code = $_POST['cap'];
    $uid = $_SESSION['user_id'];
    $date = date("Y-m-d H:i:s");
    if ($insert_stmt = $mysqli->prepare("INSERT INTO orders (date, user_id) VALUES (?, ?)")) {
       $insert_stmt->bind_param('ss', $date, $uid);
       // Esegui la query ottenuta.
       if ($insert_stmt->execute()) {
       } else {
         $mysqli->close();
         header("Location: https://www.foodonthego.altervista.org/?orderError");
       }
    }
    $cookie = isset($_COOKIE['cart_items_cookie']) ? $_COOKIE['cart_items_cookie'] : "";
    $cookie = stripslashes($cookie);
    $saved_cart_items = json_decode($cookie, true);

    $oid = mysqli_insert_id($mysqli);

      if(count($saved_cart_items)>0){
        foreach($saved_cart_items as $id=>$qt) {

        if($insert_stmt = $mysqli->prepare("INSERT INTO orders_details (order_id, product_id, quantity) VALUES (?, ?, ?)")) {
           $insert_stmt->bind_param('sss', $oid, $id, $qt['quantity']);
           // Esegui la query ottenuta.
           if ($insert_stmt->execute()) {
             //
           } else {
             $mysqli->close();
             header("Location: https://www.foodonthego.altervista.org/?orderError");
           }
                }
      }
      emptyCart();
      $mysqli->close();
      header("Location: https://www.foodonthego.altervista.org/profile.php");
    }

?>
