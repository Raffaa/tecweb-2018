<?php require_once './utils/session_helper.php'; ?>
<?php require_once './utils/db_connect.php'; ?>
<?php
    my_session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="icon" type="image/png" href="./dependancies/MaterialKitv2.0.3/assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
  Food On The Go
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="./dependancies/MaterialKitv2.0.3/assets/css/material-kit.css?v=2.0.3" rel="stylesheet" />
  <!-- Custom CSS Files  -->
  <link href="./dependancies/MaterialKitv2.0.3/assets/css/style.css" rel="stylesheet" />
</head>

<body class="index-page sidebar-collapse">
  <!-- Navigation bar -->
  <?php require_once './utils/navigation.php'?>
  <div class="page-header header-filter clear-filter purple-filter" data-parallax="true" style="background-image: url('./dependancies/MaterialKitv2.0.3/assets/img/ah.jpg');">
    <div class="container">
      <div class="row">
        <div class="col-md-8 ml-auto mr-auto">
          <div class="brand">
            <h1>Food On The Go.</h1>
            <h3>Eat Whenever And Wherever You Want!</h3>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="main main-raised">
    <!-- Notifications -->
    <?php require "./utils/notifications/notifications.php"; ?>
    <div class="section section-basic">
      <div class="container">
        <div class="title text-center">
          <h2>What Do You Want To Eat?</h2>
        <h4><small>Explore our entire catalogue.</small></h4>
      </div>
        <div class="space-50"></div>
        <ul class="nav nav-pills nav-pills-purple nav-justified">
          <li class="nav-item"><a class="nav-link active" href="#pill1" data-toggle="tab">Starter</a></li>
          <li class="nav-item"><a class="nav-link" href="#pill2" data-toggle="tab">First Course</a></li>
          <li class="nav-item"><a class="nav-link" href="#pill3" data-toggle="tab">Second Course</a></li>
          <li class="nav-item"><a class="nav-link" href="#pill4" data-toggle="tab">Desert</a></li>
          <li class="nav-item"><a class="nav-link" href="#pill5" data-toggle="tab">Drink</a></li>
        </ul>
        <div class="tab-content tab-space">
          <input class="form-control" id="myInput" type="text" placeholder="Search..">
          <br>
          <?php
          $mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
          if ($stmt = $mysqli->prepare("SELECT name, price, description, category_id, image_path, product_id, supplier_name FROM products ORDER BY category_id ASC")) {
            $stmt->execute(); // esegue la query appena creata.
            $stmt->store_result();
            $stmt->bind_result($name, $price, $description, $category_id, $image_path, $product_id, $supplier_name); // recupera il risultato della query e lo memorizza nelle relative variabili.
            if($stmt->num_rows < 1) { ?>
              </br>
              <h5 class="text-center"><mall>No Data :(</small></h5>
            <?php } else {
              $i = 0;
                      while($stmt->fetch()) {
                          if ($i != $category_id) {
                            if ($i != 0) { ?>
                            </tbody>
                          </table>
                        </div>
                            <?php }
                      ?>
                      <div class=<?php if($i==0) { echo '"tab-pane active"'; } else {echo "tab-pane";} ?> id=<?php echo "pill".$category_id ?> >
                        <table class="table table-hover">
                          <thead>
                            <tr>
                              <th scope="col" class="d-none d-md-table-cell">#</th>
                              <th scope="col">Name</th>
                              <th scope="col">Description</th>
                              <th scope="col" class="d-none d-md-table-cell">Supplier</th>
                              <th scope="col">Price</th>
                              <?php if(isset($_SESSION['user_id'])) echo '<th scope="col">Add to Cart</th>' ?>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                          }
                          $i = $category_id;
                        ?>
                      <tr>
                          <th class="d-none d-md-table-cell" scope="row"><?php echo '<img src="./uploads/'.$image_path.'" alt="meal_description_image" class="rounded-circle img-fluid" style="width:64px;height:64px;"></img>' ?></th>
                          <td style="vertical-align: middle;"><strong><?php echo $name ?><strong></td>
                          <td style="vertical-align: middle;"><?php echo $description ?></td>
                          <td class="d-none d-md-table-cell" style="vertical-align: middle;"><?php echo $supplier_name ?></td>
                          <td style="vertical-align: middle;"><?php echo $price." €" ?></td>
                          <?php if(isset($_SESSION['user_id'])) {?>
                            <td style="vertical-align: middle;"><a href="javascript:void(0);" onclick=<?php echo '"addToCart('.$product_id.');"'?>
                                <i class="material-icons"> add_shopping_cart
                                </i>
                              </a></td><?php }?>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  <?php } } ?>
              </div>
      </div>
    </div>
  </div>
</div>

  <?php require_once './utils/footer.php' ?>

  <!--   Modals   -->
  <?php require "./modals/login.php"; ?>
  <?php require "./modals/register.php"; ?>

  <!--   Core JS Files   -->
  <script src="./dependancies/MaterialKitv2.0.3/assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="./dependancies/MaterialKitv2.0.3/assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="./dependancies/MaterialKitv2.0.3/assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
  <script src="./dependancies/MaterialKitv2.0.3/assets/js/plugins/moment.min.js"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="./dependancies/MaterialKitv2.0.3/assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="./dependancies/MaterialKitv2.0.3/assets/js/material-kit.js?v=2.0.3" type="text/javascript"></script>

  <script src="./utils/script/add-to-cart.js?ver=2.0" type ="text/javascript"></script>

  <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>

  <script src="./utils/script/sha512.js"></script>

  <script>
    $(document).ready(function() {

      $("#loginForm").validate();
      $("#signupForm").validate();
  });
</script>
<script>
//FormHash
function formhash(form, password) {
   var p = document.createElement("input");
   form.appendChild(p);
   p.id = "p";
   p.name = "p";
   p.type = "hidden";
   p.value = hex_sha512(password.value);
   $("#loginPsw").removeAttr("required");
   $("#registerPsw").removeAttr("required");
   password.value = "";
}
</script>
<script>
$(function () {
    $('input[name="supplcode"]').hide();
    $('input[name="supplcode"]').prop('required',false);

    //show it when the checkbox is clicked
    $('input[name="suppl_check"]').on('click', function () {
        if ($(this).prop('checked')) {
            $('input[name="supplcode"]').fadeIn();
            $('input[name="supplcode"]').prop('required',true);
        } else {
            $('input[name="supplcode"]').hide();
            $('input[name="supplcode"]').prop('required',false);
        }
    });
});
</script>
</body>

</html>
