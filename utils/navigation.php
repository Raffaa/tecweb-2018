<?php require_once './utils/login/fb_config.php'; ?>
<?php require_once './utils/login/logout_helper.php'; ?>
<?php require_once './utils/db_connect.php'; ?>

<nav class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="100" id="sectionsNav">
  <div class="container">
    <div class="navbar-translate">
      <a class="navbar-brand" href="./index.php">
        Food On The Go </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://twitter.com/#" target="_blank" data-original-title="Follow us on Twitter">
            <i class="fa fa-twitter"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://www.facebook.com/#" target="_blank" data-original-title="Like us on Facebook">
            <i class="fa fa-facebook-square"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://www.instagram.com/#" target="_blank" data-original-title="Follow us on Instagram">
            <i class="fa fa-instagram"></i>
          </a>
        </li>
        <?php
        if(empty($_SESSION['user_id']) || empty($_SESSION['name'])) {
          ?><li class="dropdown nav-item" id="register_login">
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
              <i class="material-icons">person_pin</i> Profile
            </a>
            <div class="dropdown-menu dropdown-with-icons">
              <p class="dropdown-item" data-toggle="modal" data-target="#signupModal">
                Register
              </p>
              <p class="dropdown-item" data-toggle="modal" data-target="#loginModal">
                Login
              </p>
            </div>
          </li><?php
        } else { ?>
          <li class="nav-item" id="cart">
            <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="./cart.php" data-original-title="Explore your shopping cart!">
              <i class="material-icons">shopping_cart</i>
                        <?php
                        // count items in the cart
                        $cookie = isset($_COOKIE['cart_items_cookie']) ? $_COOKIE['cart_items_cookie'] : "";
                        $cookie = stripslashes($cookie);
                        $saved_cart_items = json_decode($cookie, true);
                        $cart_count=count($saved_cart_items);
                        ?>
                        <span class="badge badge-default" id="cart-badge"><?php echo $cart_count; ?>
                          </a>
          </li>
        </li>
        <li class="nav-item" id="logged_usr">
          <a class="nav-link" href="./profile.php">
          <?php echo 'Hi, '. $_SESSION['name'];?>
        </a>
        </li>
        </li>
        <li class="nav-item" id="logout" >
          <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href=<?php echo $logoutUrl?> data-original-title="Logout">
            <i class="material-icons">meeting_room</i>
          </a>
        </li><?php
      }
      ?>
      </ul>
    </div>
  </div>
</nav>
