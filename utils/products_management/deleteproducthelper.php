<?php require_once dirname(__DIR__).'/db_connect.php'; ?>
<?php require_once dirname(__DIR__).'/session_helper.php'; ?>
<?php
my_session_start();

$pid = $_POST['productid'];
if ($insert_stmt = $mysqli->prepare("DELETE FROM products WHERE product_id=?")) {
   $insert_stmt->bind_param('s', $pid);
   // Esegui la query ottenuta.
   if ($insert_stmt->execute()) {
     $mysqli->close();
     header("Location: https://www.foodonthego.altervista.org/profile.php");
   } else {
     $mysqli->close();
     header("Location: https://www.foodonthego.altervista.org/?addProductError");
   }
}
?>
