<?php require_once dirname(__DIR__).'/db_connect.php'; ?>
<?php require_once dirname(__DIR__).'/session_helper.php'; ?>
<?php require_once dirname(__DIR__).'/file_upload/upload.php'; ?>
<?php
my_session_start();

$name = $_POST['name'];
$price = $_POST['price'];
$desc = $_POST['desc'];
$uid = $_SESSION['user_id'];
$s_name = $_SESSION['name'];
$cat = $_POST['category'];
if ($insert_stmt = $mysqli->prepare("INSERT INTO products (name, price, description, image_path, user_id, supplier_name, category_id) VALUES (?, ?, ?, ?, ? ,?, ?)")) {
   $insert_stmt->bind_param('sssssss', $name, $price, $desc, $image_path, $uid, $s_name, $cat);
   // Esegui la query ottenuta.
   if ($insert_stmt->execute()) {
     uploadImg($image_path);
     $mysqli->close();
     header("Location: https://www.foodonthego.altervista.org/profile.php");
   } else {
     $mysqli->close();
     header("Location: https://www.foodonthego.altervista.org/?addProductError");
   }
}
?>
