<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT); //REPORT_ALL for dubug only

define("HOST", "localhost");
define("USER", "foodonthego");
define("PASSWORD", "");
define("DATABASE", "my_foodonthego");
$mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
if ($mysqli->connect_error) {
		die("Connection failed: " . $mysqli->connect_error);
	}
?>
