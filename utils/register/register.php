<?php require_once dirname(__DIR__).'/db_connect.php'; ?>
<?php
$email = $_POST['email'];
//email check
if($stmt = $mysqli->prepare("SELECT user_id FROM users WHERE email = ?")) {
  $stmt->bind_param('s', $email); // esegue il bind del parametro '$email'.
      $stmt->execute(); // esegue la query appena creata.
      $stmt->store_result();
      if($stmt->num_rows >= 1) {
        header('Location: https://www.foodonthego.altervista.org/?mailAlreadyPresent');
        die();
      } else {
      $stmt->free_result();
    }
}
$name = $_POST['name'];

if(isset($_POST['suppl_check']) && $_POST['suppl_check'] == 'true') {
  $supplier = 1;
  $supplier_id = $_POST['supplcode'];
} else {
  $supplier = 0;
  $supplier_id = NULL;
}
// Recupero la password criptata dal form di inserimento.
$password = $_POST['p'];
// Crea una chiave casuale
$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
// Crea una password usando la chiave appena creata.
$password = hash('sha512', $password.$random_salt);
// Inserisci a questo punto il codice SQL per eseguire la INSERT nel tuo database
// Assicurati di usare statement SQL 'prepared'.
if ($insert_stmt = $mysqli->prepare("INSERT INTO users (name, email, password, salt, supplier, supplier_id) VALUES (?, ?, ?, ?, ? ,?)")) {
   $insert_stmt->bind_param('ssssss', $name, $email, $password, $random_salt, $supplier, $supplier_id);
   // Esegui la query ottenuta.
   if ($insert_stmt->execute()) {
     $mysqli->close();
     header("Location: https://www.foodonthego.altervista.org/?registerSuccessfull");
   } else {
     $mysqli->close();
     header("Location: https://www.foodonthego.altervista.org/?registerError");
   }
}
//print_r($mysqli->error_list);
?>
