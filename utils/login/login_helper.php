<?php require_once dirname(__DIR__).'/session_helper.php'; ?>
<?php require_once dirname(dirname(__DIR__)).'/dependancies/FacebookPhpSdk/autoload.php';?>
<?php require_once dirname(__DIR__).'/db_connect.php'; ?>
<?php
my_session_start();
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

if (isset($_GET['email_login'])) {
  switch(emailLogin($_POST['email'], $_POST['p'], $mysqli)) {
    case -1:
        header('Location: https://www.foodonthego.altervista.org/?bruteforceWarning');
        die();
        break;
    case -2:
        header('Location: https://www.foodonthego.altervista.org/?passwordIncorrect');
        die();
        break;
    case -3:
        header('Location: https://www.foodonthego.altervista.org/?userNotFound');
        die();
        break;
    case -4:
        header('Location: https://www.foodonthego.altervista.org/?loginError');
        die();
        break;
    case 0;
        break;
  }
} else if (isset($_GET['google_login'])) {
  //
} else {
  facebookLogIn($mysqli);
}
$mysqli->close();
header("Location: https://www.foodonthego.altervista.org");
die();



/*--- Facebook LogIn Function ---*/
function facebookLogIn($mysqli) {
  $fb = new Facebook(array(
    'app_id' => '177908189552677', // Replace {app-id} with your app id
    'app_secret' => '06c2f223c1d1b57f7b941c1ec3b85077',
    'default_graph_version' => 'v2.10',
    ));

  $helper = $fb->getRedirectLoginHelper();

  try {
    if(isset($_SESSION['facebook_access_token'])){
        $accessToken = $_SESSION['facebook_access_token'];
    }else{
          $accessToken = $helper->getAccessToken();
    }
  } catch(FacebookResponseException $e) {
     echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(FacebookSDKException $e) {
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }
    if(isset($accessToken)){
      if(isset($_SESSION['facebook_access_token'])){
        $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
      }else{
        // Put short-lived access token in session
        $_SESSION['facebook_access_token'] = (string) $accessToken;

          // OAuth 2.0 client handler helps to manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();

        // Exchanges a short-lived access token for a long-lived one
        $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
        $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;

        // Set default access token to be used in script
        $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
      }
    }
    // Logged in

    try {
      // Returns a `Facebook\FacebookResponse` object
      $response = $fb->get('/me?fields=id,name,email');
    } catch(FacebookResponseException $e) {
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(FacebookSDKException $e) {
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }
    $user = $response->getGraphUser();
    //Find if user aleady logged with his fb Account
    if(($userId = getFbUser($user->getId(), $mysqli)) != null) {
      $_SESSION['user_id'] = $userId;
      $_SESSION['name'] = $user->getName();
    } else {
      if ($insert_stmt = $mysqli->prepare("INSERT INTO users (name, email, facebook_id) VALUES (?, ?, ?)")) {
         $insert_stmt->bind_param('sss', $user->getName(), $user->getEmail(), $user->getId());
         // Esegui la query ottenuta.
         if ($insert_stmt->execute()) {
           $_SESSION['user_id'] = getFbUser($user->getId(),  $mysqli);
           $_SESSION['name'] = $user->getName();
           $mysqli->close();
         } else {
           die("SQL ERROR!");
           $mysqli->close();
         }
      }
  }
  $_SESSION['usr_type'] = 'Facebook User';
    }


  function getFbUser($facebook_id, $mysqli) {
    if($stmt = $mysqli->prepare("SELECT user_id FROM users WHERE facebook_id = ?")) {
      $stmt->bind_param('s', $facebook_id); // esegue il bind del parametro '$email'.
          $stmt->execute(); // esegue la query appena creata.
          $stmt->store_result();
          $stmt->bind_result($user_id); // recupera il risultato della query e lo memorizza nelle relative variabili.
          $stmt->fetch();
          if($stmt->num_rows == 1) {
              return $user_id;
          } else {
            return null;
          }
        }
      }

function emailLogin($email, $password, $mysqli) {
   // Usando statement sql 'prepared' non sarà possibile attuare un attacco di tipo SQL injection.
   if ($stmt = $mysqli->prepare("SELECT user_id, name, password, salt , supplier FROM users WHERE email = ? LIMIT 1")) {
      $stmt->bind_param('s', $email); // esegue il bind del parametro '$email'.
      $stmt->execute(); // esegue la query appena creata.
      $stmt->store_result();
      $stmt->bind_result($user_id, $name, $db_password, $salt, $supplier); // recupera il risultato della query e lo memorizza nelle relative variabili.
      $stmt->fetch();
      $password = hash('sha512', $password.$salt); // codifica la password usando una chiave univoca.
      if($stmt->num_rows == 1) { // se l'utente esiste
         // verifichiamo che non sia disabilitato in seguito all'esecuzione di troppi tentativi di accesso errati.
         if(checkbrute($user_id, $mysqli) == true) {
            // Account disabilitato
            // Invia un e-mail all'utente avvisandolo che il suo account è stato disabilitato.
            return -1;
         } else {
         if($db_password == $password) { // Verifica che la password memorizzata nel database corrisponda alla password fornita dall'utente.
            // Password corretta!
               $user_browser = $_SERVER['HTTP_USER_AGENT']; // Recupero il parametro 'user-agent' relativo all'utente corrente.

               $user_id = preg_replace("/[^0-9]+/", "", $user_id);// ci proteggiamo da un attacco XSS
               $_SESSION['user_id'] = $user_id;
               $username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username); // ci proteggiamo da un attacco XSS
               $_SESSION['name'] = $name;
               $_SESSION['login_string'] = hash('sha512', $password.$user_browser);
               if($supplier == 1) {
                 $_SESSION['usr_type'] = 'Supplier';
               } else {
                 $_SESSION['usr_type'] = 'User';
               }
               $_SESSION['email_access_token'] = 'true';
               // Login eseguito con successo.
               return 0;
         } else {
            // Password incorretta.
            // Registriamo il tentativo fallito nel database.
            $now = time();
            $mysqli->query("INSERT INTO login_attempts (user_id, time) VALUES ('$user_id', '$now')");
            return -2;
         }
      }
      } else {
         // L'utente inserito non esiste.
         return -3;
      }
   }
   return -4;
}

function checkbrute($user_id, $mysqli) {
   // Recupero il timestamp
   $now = time();
   // Vengono analizzati tutti i tentativi di login a partire dalle ultime due ore.
   $valid_attempts = $now - (2 * 60 * 60);
   if ($stmt = $mysqli->prepare("SELECT time FROM login_attempts WHERE user_id = ? AND time > '$valid_attempts'")) {
      $stmt->bind_param('i', $user_id);
      // Eseguo la query creata.
      $stmt->execute();
      $stmt->store_result();
      // Verifico l'esistenza di più di 5 tentativi di login falliti.
      if($stmt->num_rows > 5) {
         return true;
      } else {
         return false;
      }
   }
}

?>
