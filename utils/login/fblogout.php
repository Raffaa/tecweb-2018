<?php require_once './fb_config.php'; ?>
<?php include_once '../session_helper.php'; ?>
<?php
my_session_start();

// read
$cookie = $_COOKIE['cart_items_cookie'];
$cookie = stripslashes($cookie);
$saved_cart_items = json_decode($cookie, true);

// remove values
unset($saved_cart_items);

// delete cookie value
unset($_COOKIE["cart_items_cookie"]);

// empty value and expiration one hour before
setcookie("cart_items_cookie", "", time() - 3600);

// enter empty value
$json = json_encode($saved_cart_items, true);
setcookie("cart_items_cookie", $json, time() + (86400 * 30), '/'); // 86400 = 1 day
$_COOKIE['cart_items_cookie']=$json;

// Remove access token from session
unset($_SESSION['facebook_access_token']);
unset($_SESSION['name']);
unset($_SESSION['user_id']);
unset($_SESSION['email_access_token']);
$_SESSION = array();
session_destroy();


// Remove user data from session

// Redirect to the homepage
header("Location: https://www.foodonthego.altervista.org");
?>
