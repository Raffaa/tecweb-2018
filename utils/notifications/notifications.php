<?php if(isset($_GET['registerError'])) {?>
  <div class="alert alert-danger">
             <div class="container">
                 <div class="alert-icon">
                    <i class="material-icons">error_outline</i>
                </div>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="material-icons">clear</i></span>
                </button>
                 <b>Error Alert:</b> Ouch! You screwed up the server this time. You should find a good excuse for your Boss...
            </div>
        </div>
<?php }
if(isset($_GET['registerSuccessfull'])) { ?>
  <div class="alert alert-success">
            <div class="container">
                <div class="alert-icon">
                    <i class="material-icons">check</i>
                </div>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="material-icons">clear</i></span>
                </button>
                <b>Registration was Successfull!:</b> Yuhuuu! You can now login!
            </div>
        </div>
<?php }
if(isset($_GET['mailAlreadyPresent'])) { ?>
  <div class="alert alert-danger">
             <div class="container">
                 <div class="alert-icon">
                    <i class="material-icons">error_outline</i>
                </div>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="material-icons">clear</i></span>
                </button>
                 <b>Error Alert:</b> Ouch! The mail you gave is already present in our database.
            </div>
        </div>
<?php }
if(isset($_GET['bruteforceWarning'])) { ?>
  <div class="alert alert-danger">
         <div class="container">
            <div class="alert-icon">
                <i class="material-icons">error_outline</i>
            </div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="material-icons">clear</i></span>
            </button>
             <b>Error Alert:</b> Hey Stop! Too many login attempts. Retry later.
        </div>
    </div>
<?php }
  if(isset($_GET['passwordIncorrect'])) { ?>
    <div class="alert alert-danger">
       <div class="container">
         <div class="alert-icon">
           <i class="material-icons">error_outline</i>
         </div>
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true"><i class="material-icons">clear</i></span>
          </button>
             <b>Error Alert:</b> Ouch! Password and email don't match. Retry!
          </div>
    </div>
<?php }
  if(isset($_GET['loginError'])) { ?>
    <div class="alert alert-danger">
       <div class="container">
           <div class="alert-icon">
              <i class="material-icons">error_outline</i>
            </div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true"><i class="material-icons">clear</i></span>
            </button>
             <b>Error Alert:</b> Ouch!! Something went wrong during login. Please retry!
      </div>
    </div>
<?php }
  if(isset($_GET['userNotFound'])) { ?>
    <div class="alert alert-danger">
       <div class="container">
         <div class="alert-icon">
           <i class="material-icons">error_outline</i>
         </div>
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true"><i class="material-icons">clear</i></span>
          </button>
             <b>Error Alert:</b> Ouch! This email is not present in our database. Please register first!
          </div>
    </div>
<?php }
if(isset($_GET['orderError'])) { ?>
  <div class="alert alert-danger">
     <div class="container">
         <div class="alert-icon">
            <i class="material-icons">error_outline</i>
          </div>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true"><i class="material-icons">clear</i></span>
          </button>
           <b>Error Alert:</b> Ouch!! Something went wrong during checkout. Please retry!
    </div>
  </div>
<?php } ?>
