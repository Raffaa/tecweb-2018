<?php
// read
$cookie = isset($_COOKIE['cart_items_cookie']) ? $_COOKIE['cart_items_cookie'] : "";
$cookie = stripslashes($cookie);
$saved_cart_items = json_decode($cookie, true);

if(isset($_GET['removeProduct'])){
  $id = $_GET['productId'];
  unset($saved_cart_items[$id]);

// delete cookie value
unset($_COOKIE["cart_items_cookie"]);

// empty value and expiration one hour before
setcookie("cart_items_cookie", "", time() - 3600);

// enter new value
$json = json_encode($saved_cart_items, true);
setcookie("cart_items_cookie", $json, time() + (86400 * 30), '/'); // 86400 = 1 day
$_COOKIE['cart_items_cookie']=$json;
die('Died');
}
if(!empty($_POST['pid'])) {
  $id = $_POST['pid'];
  $quantity = $saved_cart_items[$id]['quantity'];
  $quantity += $_POST['mode'];

  $quantity = $quantity < 1 ? 1 : $quantity;

  var_dump($quantity);
    // remove the item from the array
  unset($saved_cart_items[$id]);

  // delete cookie value
  setcookie("cart_items_cookie", "", time()-3600);

  // add the item with updated quantity
  $saved_cart_items[$id]=array(
      'quantity'=>$quantity
  );

  // enter new value
  $json = json_encode($saved_cart_items, true);
  setcookie("cart_items_cookie", $json, time() + (86400 * 30), '/'); // 86400 = 1 day
  $_COOKIE['cart_items_cookie']=$json;
  die('Died');
}
$id = $_POST['productId'];;
$quantity= 1;

// add new item on array
$cart_items[$id]=array(
    'quantity'=>$quantity
);

// if $saved_cart_items is null, prevent null error
if(!$saved_cart_items){
    $saved_cart_items=array();
}

// check if the item is in the array, if it is, do not add
if(array_key_exists($id, $saved_cart_items)){
    // redirect to product list and tell the user it was added to cart
    exit();
}

// else, add the item to the array
else{
    // if cart has contents
    if(count($saved_cart_items)>0){
        foreach($saved_cart_items as $key=>$value){
            // add old item to array, it will prevent duplicate keys
            $cart_items[$key]=array(
                'quantity'=>$value['quantity']
            );
        }
    }

    // put item to cookie
    $json = json_encode($cart_items, true);
    setcookie("cart_items_cookie", $json, time() + (86400 * 30), '/'); // 86400 = 1 day
    $_COOKIE['cart_items_cookie']=$json;

}
die();
?>
