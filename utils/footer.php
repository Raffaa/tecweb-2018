<footer class="footer" data-background-color="black">
  <div class="container">
    <nav class="float-left">
      <ul>
        <li>
          <a href="#">
            About Us
          </a>
        </li>
        <li>
          <a href="#">
            Licenses
          </a>
        </li>
        <li class="dropdown nav-item">
          <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
            <img src="./dependancies/MaterialKitv2.0.3/assets/img/italy.svg"  alt="Lang" height="24" width="24"></img>
          </a>
          <div class="dropdown-menu dropdown-with-icons">
            <a href="./index.php" class="dropdown-item">
              <img src="./dependancies/MaterialKitv2.0.3/assets/img/italy.svg" alt="IT lang" height="24" width="24"></img>
              <p style="padding-left: 5px;" > Italiano</p>
            </a>
            <a href="./index.php" class="dropdown-item">
              <img src="./dependancies/MaterialKitv2.0.3/assets/img/uk.svg" alt="UK lang" height="24" width="24"></img>
              <p style="padding-left: 5px"> English</p>
            </a>
          </div>
        </li>
      </ul>
    </nav>
    <div class="copyright float-right">
      &copy;
      <?php echo date("Y"); ?>
      , made with <i class="material-icons">favorite</i> by Raffaelli Simone using
      <a href="https://www.creative-tim.com" target="_blank">Material Kit</a>
    </div>
  </div>
</footer>
