<?php require_once './utils/session_helper.php'; ?>
<?php require_once './utils/db_connect.php'; ?>
<?php
    my_session_start();

    function categoryName($category_id) {
      switch($category_id){
        case 1:
              return 'Starter';
              break;
        case 2:
        return 'First Course';
        break;
        case 3:
        return 'Second Course';
        break;
        case 4:
        return 'Dessert';
        break;
        case 5:
        return 'Drink';
        break;
      }
    }
    ?>
<?php
if (isset($_SESSION['user_id'])) {
  ?>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="./dependancies/MaterialKitv2.0.3/assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
    Food On The Go - Profile
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="./dependancies/MaterialKitv2.0.3/assets/css/material-kit.css?v=2.0.3" rel="stylesheet" />
    <!-- Custom CSS Files  -->
    <link href="./dependancies/MaterialKitv2.0.3/assets/css/style.css" rel="stylesheet" />
  </head>
  <body class="profile-page sidebar-collapse">
  <?php require_once './utils/navigation.php'?>
  <div class="page-header header-filter" data-parallax="true" style="background-image: url('./dependancies/MaterialKitv2.0.3/assets/img/profile_city.jpg');"></div>
    <div class="main main-raised">
      <div class="profile-content">
        <div class="container">
          <div class="row">
            <div class="col-md-6 ml-auto mr-auto">
              <div class="profile">
                <div class="avatar">
                  <img src="./dependancies/MaterialKitv2.0.3/assets/img/avatar.png" alt="Circle Image" class="img-raised rounded-circle img-fluid">
                </div>
                <div class="name">
                  <h3 class="title"><?php echo $_SESSION['name'] ?></h3>
                  <h6><?php echo $_SESSION['usr_type'] ?></h6>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-10 ml-auto mr-auto">
              <h4 class="text-center">Recent Orders</h4>
              <?php
              $mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
              if ($stmt = $mysqli->prepare("SELECT order_id, date FROM orders WHERE user_id = ? ORDER BY order_id ASC")) {
                $stmt->bind_param('s', $_SESSION['user_id']);
                $stmt->execute(); // esegue la query appena creata.
                $stmt->store_result();
                $stmt->bind_result($oid, $uid); // recupera il risultato della query e lo memorizza nelle relative variabili.
                if($stmt->num_rows < 1) { ?>
                  </br>
                  <h5 class="text-center"><mall>No Data :(</small></h5>
                <?php } else { ?>
                        <table class="table table-hover">
                          <thead>
                            <tr>
                              <th scope="col">Order #</th>
                              <th scope="col">Date</th>
                              <th scope="col">Info</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php
                          $i = 0;
                          while($stmt->fetch()) {
                          $i++; ?>
                          <tr>
                              <td style="vertical-align: middle;"><p><?php echo $oid ?></p></td>
                              <td style="vertical-align: middle;"><p><?php echo $uid ?></p></td>
                              <td style="vertical-align: middle;">
                                <button class="btn btn-primary btn-link" onclick=<?php echo '"showInfo('.$oid.')"'?>>
                                  <i class="material-icons">filter_list</i>
                                </button>
                              </td>
                            </tr>
                          <?php } ?>
                          </tbody>
                        </table>
                      <?php } } ?>
                      <div class="space-50"></div>
                      <?php

              if (isset($_SESSION['usr_type']) && $_SESSION['usr_type'] == 'Supplier') { ?>
                <h4 class="text-center">Your Product On Sale</h4>
                <?php
                $mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
                if ($stmt = $mysqli->prepare("SELECT name, price, description, category_id, product_id FROM products WHERE user_id = ?")) {
                  $stmt->bind_param('s', $_SESSION['user_id']);
                  $stmt->execute(); // esegue la query appena creata.
                  $stmt->store_result();
                  $stmt->bind_result($name, $price, $description, $category_id, $product_id); // recupera il risultato della query e lo memorizza nelle relative variabili.
                  if($stmt->num_rows < 1) { ?>
                    </br>
                    <h5 class="text-center"><mall>No Data :(</small></h5>
                  <?php } else { ?>
                          <table class="table table-hover">
                            <thead>
                              <tr>
                                <th class="d-none d-md-table-cell" scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Description</th>
                                <th class="d-none d-md-table-cell" scope="col">Category</th>
                                <th scope="col">Price</th>
                                <th scope="col">Edit</th>
                              </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 0;
                            while($stmt->fetch()) {
                            $i++; ?>
                            <tr>
                                <th class="d-none d-md-table-cell" scope="row"><?php echo $i ?></th>
                                <td><?php echo $name ?></td>
                                <td><?php echo $description ?></td>
                                <td class="d-none d-md-table-cell"><?php echo categoryName($category_id) ?></td>
                                <td><?php echo $price ?></td>
                                <td>
                                  <a href=<?php echo '"./editproduct.php?pid='.urlencode($product_id).'&name='.urlencode($name).'&desc='.urlencode($description).'&cat='.$category_id.'&price='.$price.'"' ?> >
                                  <i class="material-icons">edit</i>
                                </a></td>
                              </tr>
                            <?php } ?>
                            </tbody>
                          </table>
                        <?php } } ?>
                        <div class="text-center">
                          <a href="./addproduct.php">
                        <button class="btn btn-primary btn-round">
                          <i class="material-icons">add</i> Add New Product
                        </button>
                      </a>
                      </div>
                    <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php require_once './utils/footer.php' ?>

    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="orderModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <table class="table table-hover">
        <tr>
          <th>Product</th>
          <th>Quantity</th>
        </tr>
        <tbody id="orderInfo">
          <!-- ... -->
        </tbody>
      </table>

    </div>
  </div>
</div>

    <!--   Core JS Files   -->
    <script src="./dependancies/MaterialKitv2.0.3/assets/js/core/jquery.min.js" type="text/javascript"></script>
    <script src="./dependancies/MaterialKitv2.0.3/assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="./dependancies/MaterialKitv2.0.3/assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
    <script src="./dependancies/MaterialKitv2.0.3/assets/js/plugins/moment.min.js"></script>
    <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
    <script src="./dependancies/MaterialKitv2.0.3/assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
    <script src="./dependancies/MaterialKitv2.0.3/assets/js/material-kit.js?v=2.0.3" type="text/javascript"></script>

    <script>
      function showInfo(id) {
        $.ajax({
          type:"POST",
          url:'https://www.foodonthego.altervista.org/utils/order/orderdetail.php/',
          data: {id: id},
          success:function(response){
                 // refresh badge
                 $("#orderInfo").html(response);
                 $("#orderModal").modal();

          },
          error: function() {
            alert('AAHAH');
          }
       });
      }
    </script>

  </body>
  </html>
<?php } else {
  header("Location: https://www.foodonthego.altervista.org/unauthorized.php");
  }
?>
