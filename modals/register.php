<div class="modal fade" id="signupModal" tabindex="-1" role="">
    <div class="modal-dialog modal-login" role="document">
        <div class="modal-content">
            <div class="card card-signup card-plain">
                <div class="modal-header">
                    <div class="card-header card-header-primary text-center">
                        <h4 class="card-title">Register</h4>
                        <div class="social-line">
                            <a id="FbLoginBtnR" class="btn btn-just-icon btn-link" href="<?php echo $fbLoginUrl ?>" >
                                <i class="fa fa-facebook-square"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <form class="form" method="POST" action="./utils/register/register.php" id="signupForm" onsubmit="formhash(this, this.psw)">
                        <p class="description text-center">Or Be Classical</p>
                        <div class="card-body">

                          <div class="form-group bmd-form-group">
                              <div class="input-group">
                                  <span class="input-group-addon">
                                      <i class="material-icons">face</i>
                                  </span>
                                  <input type="text" class="form-control" placeholder="Name..." name="name" required>
                              </div>
                          </div>

                            <div class="form-group bmd-form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">email</i>
                                    </span>
                                    <input type="email" class="form-control" placeholder="Email..." name="email" required>
                                </div>
                            </div>

                            <div class="form-group bmd-form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">lock_outline</i>
                                    </span>
                                    <input type="password" class="form-control" placeholder="Password..." name="psw" id="registerPsw" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}">
                                </div>
                            </div>

                            <div class="row">
                              <div class="col">
                                <div class="form-check">
                                   <label class="form-check-label">
                                       <input class="form-check-input" type="checkbox" value="true" name="suppl_check">
                                       Do you are a supplier?
                                       <span class="form-check-sign">
                                         <span class="check"></span>
                                       </span>
                                   </label>
                                 </div>
                              </div>
                              <div class="col">
                              </br>
                                <div class="input-group">
                                  <input type="number" class="form-control" placeholder="P.IVA..." name="supplcode" required>
                                </div>
                              </div>
                            </div>

                            <div class="modal-footer justify-content-center">
                              <button type="submit" class="btn btn-primary btn-link btn-wd btn-lg">Register</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
