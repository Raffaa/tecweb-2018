<div class="modal fade" id="loginModal" tabindex="-1" role="">
    <div class="modal-dialog modal-login" role="document">
        <div class="modal-content">
            <div class="card card-signup card-plain">
                <div class="modal-header">
                    <div class="card-header card-header-primary text-center">
                        <h4 class="card-title">Log in</h4>
                        <div class="social-line">
                            <a id="FbLoginBtnL" class="btn btn-just-icon btn-link" href="<?php echo $fbLoginUrl ?>" >
                                <i class="fa fa-facebook-square"></i>
                            </a>

                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <form class="form" method="POST" action="./utils/login/login_helper.php?email_login" id="loginForm" onsubmit="formhash(this, this.psw)">
                        <p class="description text-center">Or Be Classical</p>
                        <div class="card-body">

                            <div class="form-group bmd-form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">email</i>
                                    </span>
                                    <input type="email" class="form-control" placeholder="Email..." name="email" required>
                                </div>
                            </div>
                            <div class="form-group bmd-form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">lock_outline</i>
                                    </span>
                                    <input type="password" class="form-control" placeholder="Password..." name="psw" id="loginPsw" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}">
                                </div>
                            </div>
                            <div class="modal-footer justify-content-center">
                              <button type="submit" class="btn btn-primary btn-link btn-wd btn-lg">Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
