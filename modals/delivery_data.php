<div class="modal fade" id="deliveryModal" tabindex="-1" role="">
    <div class="modal-dialog modal-login" role="document">
        <div class="modal-content">
            <div class="card card-signup card-plain">
                <div class="modal-header">
                    <div class="card-header card-header-primary text-center">
                        <h4 class="card-title">Insert your delivery information</h4>
                    </div>
                </div>
                <div class="modal-body">
                    <form class="form" method="POST" action="./checkout.php" id="deliveryForm">
                        <p class="description text-center">Please fill all fileds:</p>
                        <div class="card-body">
                          <div class="form-group bmd-form-group">
                              <div class="input-group">
                                  <span class="input-group-addon">
                                      <i class="material-icons">place</i>
                                  </span>
                                  <input type="text" class="form-control" placeholder="Address..." name="address" required>
                              </div>
                          </div>

                            <div class="form-group bmd-form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">location_city</i>
                                    </span>
                                    <input type="text" class="form-control" placeholder="City..." name="city" required>
                                </div>
                            </div>

                            <div class="form-group bmd-form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">phone_iphone</i>
                                    </span>
                                    <input type="number" class="form-control" placeholder="Phone Number..." name="cell"  required>
                                </div>
                            </div>

                            <div class="form-group bmd-form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">my_location</i>
                                    </span>
                                    <input type="number" class="form-control" placeholder="Postal Code..." name="cap" required>
                                </div>
                            </div>

                            <div class="modal-footer justify-content-center">
                              <button type="submit" class="btn btn-primary btn-link btn-wd btn-lg">Pay and Order</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
