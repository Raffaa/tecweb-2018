<?php require_once './utils/session_helper.php'; ?>
<?php
    my_session_start();
?>
<?php
if (isset($_SESSION['user_id']) && isset($_SESSION['usr_type']) && $_SESSION['usr_type'] == 'Supplier') {
  ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="icon" type="image/png" href="./dependancies/MaterialKitv2.0.3/assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
  Food On The Go - Add Product
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="./dependancies/MaterialKitv2.0.3/assets/css/material-kit.css?v=2.0.3" rel="stylesheet" />
  <!-- Custom CSS Files  -->
  <link href="./dependancies/MaterialKitv2.0.3/assets/css/style.css" rel="stylesheet" />
</head>

<body class="profile-page sidebar-collapse">
  <?php require_once './utils/navigation.php'?>
  <div class="page-header header-filter" data-parallax="true" style="background-image: url('./dependancies/MaterialKitv2.0.3/assets/img/profile_city.jpg');"></div>
  <div class="main main-raised">
      <div class="container">
        <div class="space-50"></div>
        <form method="POST" action="./utils/products_management/addproducthelper.php" id="addProductForm" enctype="multipart/form-data">
          <div class="form-group">
          <label for="desc">Name</label>
          <input name="name" type="text" class="form-control" id="name" placeholder="Meal presentation name." required>
          </div>
          <div class="form-group">
            <label for="desc">Description</label>
            <textarea name="desc" class="form-control" id="desc" rows="3" placeholder="Meal description." required></textarea>
          </div>
          <div class="form-row">
          <div class="form-group col-md-8">
            <label for="price">Price</label>
            <input type="number" step="0.01" class="form-control" id="price" name="price" placeholder="Price." required>
          </div>
          <div class="form-group col-md-4">
            <label for="category">Category</label>
            <select id="category" name="category" class="form-control">
              <option value=1>Starter</option>
              <option value=2>First Course</option>
              <option value=3>Second Course</option>
              <option value=4>Dessert</option>
              <option value=5>Drink</option>
            </select>
          </div>
        </div>
        <div class="form-row">
        <div class="">
        <span class="btn btn-raised btn-round btn-default btn-file">
            <input type="file" id="file" name="file" />
        </span>
        </div>
        <span id="thumb" class="d-flex flex-row-reverse">
        </span>
        </div>
          <button type="submit" class="btn btn-primary">Add</button>
          </form>
          <div class="space-50"></div>

      </div>
  </div>

  <?php require_once './utils/footer.php' ?>

  <!--   Core JS Files   -->
  <script src="./dependancies/MaterialKitv2.0.3/assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="./dependancies/MaterialKitv2.0.3/assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="./dependancies/MaterialKitv2.0.3/assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
  <script src="./dependancies/MaterialKitv2.0.3/assets/js/plugins/moment.min.js"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="./dependancies/MaterialKitv2.0.3/assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="./dependancies/MaterialKitv2.0.3/assets/js/material-kit.js?v=2.0.3" type="text/javascript"></script>

<script>
function handleFileSelect(evt) {
  var files = evt.target.files;

  // Loop through the FileList and render image files as thumbnails.
  for (var i = 0, f; f = files[i]; i++) {

    // Only process image files.
    if (!f.type.match('image.*')) {
      continue;
    }

    var reader = new FileReader();

    // Closure to capture the file information.
    reader.onload = (function(theFile) {
      return function(e) {
        // Render thumbnail.
        var span = document.createElement('span');
        span.innerHTML =
        [
          '<img class="rounded-circle img-fluid"; style="height: 75px;  margin: 5px" src="',
          e.target.result,
          '" title="', escape(theFile.name),
          '"/>'
        ].join('');

        document.getElementById('thumb').innerHTML = span.innerHTML;
      };
    })(f);

    // Read in the image file as a data URL.
    reader.readAsDataURL(f);
  }
}

document.getElementById('file').addEventListener('change', handleFileSelect, false);

</script


</body>

</html>
<?php
} else {
  header("Location: https://www.foodonthego.altervista.org/unauthorized.php");
  }

 ?>
